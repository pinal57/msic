﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSICBusinessLogic.DB;
using System.Data.SqlClient;

namespace MSICBusinessLogic
{
    public class SearchResult_DAL
    {
        public IEnumerable<SearchResult> GetSearchResult(string SearchCriteria)
        {
            MSICEntities context = new MSICEntities();
            IEnumerable<SearchResult> list = null;

            try
            {
                list = context.Database.SqlQuery<SearchResult>("EXEC SP_MSIC_Result_Get @SEARCHCRITERIA",
                                new SqlParameter("@SEARCHCRITERIA", SearchCriteria)).ToList();
            }
            catch (Exception ex)
            {
                list = null;
            }

            return list;
        }
    }
}
