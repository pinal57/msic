﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSICApp_Test.Class
{
    public class SearchCriteria
    {
        public string Contains { get; set; }
        public string Within { get; set; }
        public string Paid { get; set; }
        public string IdDocs { get; set; }
        public string OpNeed { get; set; }
    }
}