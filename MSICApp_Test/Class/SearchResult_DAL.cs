﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MSICApp_Test.Class;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using log4net;

namespace MSICApp_Test.Class
{
    public class SearchResult_DAL
    {
        ILog Log = LogManager.GetLogger(typeof(SearchResult_DAL));
        string ConnectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["MSICConnection"]);

        public List<SearchResult> GetSearchResult(SearchCriteria objSearchCriteria)
        {
            List<SearchResult> iList = new List<SearchResult>();

            string strCriteria = "";
            string WithInValue = "";
            if (objSearchCriteria.Contains != "")
                strCriteria = " ( MU.Id LIKE '%" + objSearchCriteria.Contains + "%' OR MU.CardNumber LIKE '%" + objSearchCriteria.Contains + "%' OR MU.GivenNames LIKE '%" + objSearchCriteria.Contains + "%' OR MU.Surname LIKE '%" + objSearchCriteria.Contains + "%' ) ";

            if (objSearchCriteria.Within == "")
                WithInValue = "-120";
            else
                WithInValue = objSearchCriteria.Within;

            if (strCriteria == "")
                strCriteria = " MU.ApplicationDate BETWEEN DATEADD(DD, " + WithInValue + ", GETDATE()) AND GETDATE() ";
            else
                strCriteria = strCriteria + " AND MU.ApplicationDate BETWEEN DATEADD(DD, " + WithInValue + ", GETDATE()) AND GETDATE() ";

            if (objSearchCriteria.Paid == "1")
                strCriteria = strCriteria + " AND MU.PaymentDate IS NOT NULL AND MU.PaymentDate != '1900-01-01 00:00:00.000' ";
            else if (objSearchCriteria.Paid == "0")
                strCriteria = strCriteria + " AND (MU.PaymentDate IS NULL OR MU.PaymentDate = '1900-01-01 00:00:00.000') ";

            if (objSearchCriteria.IdDocs == "1")
                strCriteria = strCriteria + " AND MSID.DocumentIdentifier in (9, 10) AND MSID.AcceptedOn IS NOT NULL ";
            else if (objSearchCriteria.IdDocs == "0")
                strCriteria = strCriteria + " AND (MU.[TypeOfWorkCompany] IS NOT NULL AND MU.[PortMostVisited] IS NOT NULL)  ";

            if (objSearchCriteria.OpNeed == "1")
                strCriteria = strCriteria + " AND MSID.AcceptedOn IS NOT NULL AND ( MSID.DocumentIdentifier in (11) OR (MU.[TypeOfWorkCompany] IS NOT NULL AND MU.[PortMostVisited] IS NOT NULL) )  ";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd;
                SqlDataReader reader;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                string sql = "SELECT MU.Id, MU.CardNumber,	MU.GivenNames, MU.Surname FROM MsicUser MU INNER JOIN MSICIdentityDocument MSID ON MU.ID = MSID.ApplicationId ";

                if (strCriteria != "")
                    sql = sql + " WHERE  " + strCriteria;

                try
                {
                    SearchResult objSearchResult = new SearchResult();
                    cmd = new SqlCommand(sql, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        objSearchResult = new SearchResult();
                        objSearchResult.Id = Convert.ToInt32(reader["Id"]);
                        objSearchResult.CardNumber = Convert.ToString(reader["CardNumber"]);
                        objSearchResult.GivenNames = Convert.ToString(reader["GivenNames"]);
                        objSearchResult.Surname = Convert.ToString(reader["Surname"]);

                        iList.Add(objSearchResult);
                    }
                    reader.Close();
                    cmd.Dispose();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Error. Could not retrieve data. Source: {0}, Message: {1}", "GetSearchResult()", ex.Message), ex);
                }
            }

            return iList;
        }
    }
}