﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSICApp_Test.Class
{
    public class SearchResult
    {
        public int Id { get; set; }
        public string CardNumber { get; set; }
        public string GivenNames { get; set; }
        public string Surname { get; set; }
    }
}