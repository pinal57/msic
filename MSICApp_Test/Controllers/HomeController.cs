﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using MSICApp_Test.Class;


namespace MSICApp_Test.Controllers
{
    public class HomeController : Controller
    { 
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult SearchResult(string txtContains, string ddlwithin, string ddlPaid, string ddlIdDocs, string ddlOpNeed)
        {            
            SearchResult_DAL objSearchDAL = new SearchResult_DAL();
            SearchCriteria objSearch = new SearchCriteria();
            objSearch.Contains = txtContains;
            objSearch.Within = ddlwithin;
            objSearch.Paid = ddlPaid;
            objSearch.IdDocs = ddlIdDocs;
            objSearch.OpNeed = ddlOpNeed;

            List<SearchResult> iList =  objSearchDAL.GetSearchResult(objSearch);            
            return View("Index", iList);
        }
    }
}