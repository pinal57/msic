﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MSICApp_Test.Startup))]
namespace MSICApp_Test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
