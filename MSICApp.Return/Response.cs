﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MSICApp.Return
{
    public class Response <T>
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
        public string ExceptionStackTrace { get; set; }
        public T Content { get; set; }
    }

    public class Response 
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
        public string ExceptionStackTrace { get; set; }
    }
}
