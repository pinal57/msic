INSERT INTO MsicPreviousNames (ApplicationId, NameId, Title, MiddleName, GivenName,Surname)
SELECT ID, 1, CASE WHEN RTRIM(LTRIM([PrevTitle])) = '' THEN NULL ELSE [PrevTitle] END, 
[PrevOtherGivenName], [PrevFirstGivenName], [PrevSurname] FROM MsicUser  
WHERE [PrevFirstGivenName] IS NOT NULL AND [PrevSurname]  IS NOT NULL