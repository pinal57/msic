USE [MSIC]
GO

/****** Object:  Table [dbo].[MsicPreviousNames]    Script Date: 03-23-2018 12:06:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MsicPreviousNames](
	[ApplicationId] [int] NOT NULL,
	[NameId] [int] NOT NULL,
	[Title] [nvarchar](8) NULL,
	[GivenName] [nvarchar](200) NULL,
	[MiddleName] [nvarchar](20) NULL,
	[Surname] [nvarchar](100) NULL,
 CONSTRAINT [PK_MsicPreviousNames] PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC,
	[NameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MsicPreviousNames]  WITH CHECK ADD  CONSTRAINT [FK_MsicPreviousNames_MsicUser] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[MsicUser] ([ID])
GO

ALTER TABLE [dbo].[MsicPreviousNames] CHECK CONSTRAINT [FK_MsicPreviousNames_MsicUser]
GO


