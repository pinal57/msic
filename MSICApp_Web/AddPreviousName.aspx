﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddPreviousName.aspx.cs" Inherits="MSICApp_Web.AddPreviousName" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="pd-20">

                <div class="form-group">
                    <asp:Label runat="server" ID="lblMessage" ForeColor="Red" CssClass="control-label col-xs-6"></asp:Label>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-4">Title</label>
                    <div class="col-xs-10">
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-4"><span style="color:red">*</span>Given Name</label>
                    <div class="col-xs-10">
                        <asp:TextBox ID="txtGivenName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-4">Middle Name</label>
                    <div class="col-xs-10">
                        <asp:TextBox ID="txtMiddleName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-4"><span style="color:red">*</span>Surname</label>
                    <div class="col-xs-10">
                        <asp:TextBox ID="txtSurname" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-offset-2 col-xs-10">
                        <asp:Button runat="server" ID="btnSave" Text="Save" ClientIDMode="Static" OnClick="btnSave_Click" CssClass="btn btn-primary" />
                         <asp:Button runat="server" ID="btnCancel" Text="Cancel" ClientIDMode="Static" OnClick="btnCancel_Click" CssClass="btn btn-default" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
