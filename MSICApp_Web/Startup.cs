﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MSICApp_Web.Startup))]
namespace MSICApp_Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
