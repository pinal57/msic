﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MSICApp.Return;
using MSICApp_Web.MSICUserPreviousData;
using System.Web.Services;

namespace MSICApp_Web
{
    public partial class ListPreviousName : System.Web.UI.Page
    {
        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["ApplicationId"] = "4";
            if (!IsPostBack)
            {
                BindGridView();
            }
        }

        protected void btnAddPreviousName_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddPreviousName.aspx");
        }

        #endregion

        #region Page Method
        public void BindGridView()
        {
            Response<List<MsicPreviousName>> FinalRes = GetPreviousNameData(Convert.ToInt32(Session["ApplicationId"]));

            grvPreviousNameList.DataSource = FinalRes.Content;
            grvPreviousNameList.DataBind();
            if (FinalRes.Content.Count() > 0)
                ControlEnableDisable(false);
            else
                ControlEnableDisable(true);
        }

        public Response<List<MsicPreviousName>> GetPreviousNameData(Int32 ApplicationId)
        {
            Read objRead = new Read();
            Response<List<MsicPreviousName>> res = objRead.GetPreviousNameListByApplicationId(ApplicationId);

            switch (res.StatusCode)
            {
                case 200:
                    break;
                case 404:
                    return new Response<List<MsicPreviousName>>
                    {
                        StatusCode = 404,
                        Message = res.Message,
                        Content = null
                    };
                case -99:
                default:
                    return new Response<List<MsicPreviousName>>
                    {
                        StatusCode = -99,
                        Message = res.Message,
                        Content = null
                    };
            }

            return res;
        }

        protected void chkAppHasPreviosName_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAppHasPreviosName.Checked == true)
                divAddNewPreviousName.Visible = true;
            else
                divAddNewPreviousName.Visible = false;
        }

        public void ControlEnableDisable(bool flag)
        {
            chkAppHasPreviosName.Enabled = flag;
            chkAppHasPreviosName.Checked = !flag;
            divAddNewPreviousName.Visible = !flag;
        }
        #endregion

        #region Web Method
        [WebMethod]
        public static string DeletePreviousData(string NameId)
        {
            string RetVal = "true";
            Write objWrite = new Write();

            Response res = objWrite.DeletePreviousName(Convert.ToInt32(HttpContext.Current.Session["ApplicationId"]), Convert.ToInt32(NameId));

            if (res.StatusCode != 200)
            {
                RetVal = "false";
            }

            return RetVal;
        }
        #endregion
    }
}