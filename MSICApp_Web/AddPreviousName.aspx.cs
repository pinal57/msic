﻿using System;
using MSICApp.Return;
using MSICApp_Web.MSICUserPreviousData;

namespace MSICApp_Web
{
    public partial class AddPreviousName : System.Web.UI.Page
    { 
        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString.Count > 0)
                {
                    if (Request.QueryString["NameID"] != null)
                    {                        
                        Read objRead = new Read();
                        Response<MsicPreviousName> res = objRead.GetPreviousNameListByAppIdAndNameId(Convert.ToInt32(Session["ApplicationId"]),
                                                                                            Convert.ToInt32(Request.QueryString["NameID"]));

                        if (res.Content != null)
                        {
                            txtTitle.Text = Convert.ToString(res.Content.Title);
                            txtGivenName.Text = Convert.ToString(res.Content.GivenName);
                            txtMiddleName.Text = Convert.ToString(res.Content.MiddleName);
                            txtSurname.Text = Convert.ToString(res.Content.Surname);
                            btnSave.Text = "Update";
                        }
                    }
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["NameID"] != null)
                Save(true);
            else
                Save(false);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ListPreviousName.aspx");
        }
        #endregion

        #region Page Method
        public void Save(bool IsUpdate)
        {
            if (txtGivenName.Text.ToString().Trim() != "" && txtSurname.Text.ToString().Trim() != "")
            {
                Write objWrite = new Write();
                MsicPreviousName objMsicPreviousName = new MsicPreviousName();

                objMsicPreviousName.ApplicationId = Convert.ToInt32(Session["ApplicationId"]);
                objMsicPreviousName.Title = txtTitle.Text.ToString();
                objMsicPreviousName.GivenName = txtGivenName.Text.ToString();
                objMsicPreviousName.MiddleName = txtMiddleName.Text.ToString();
                objMsicPreviousName.Surname = txtSurname.Text.ToString();

                if (IsUpdate)
                    objMsicPreviousName.NameId = Convert.ToInt32(Request.QueryString["NameID"]);
                else
                    objMsicPreviousName.NameId = 0;

                Response FinalRes = objWrite.SavePreviousName(objMsicPreviousName);

                if (FinalRes.StatusCode == 200)
                    Response.Redirect("ListPreviousName.aspx");
                else
                    lblMessage.Text = FinalRes.Message;
            }
            else
                lblMessage.Text = "Please enter Given Name and Surname.";
        }
        #endregion

    }
}