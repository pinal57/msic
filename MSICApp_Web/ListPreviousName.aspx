﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListPreviousName.aspx.cs"
    Inherits="MSICApp_Web.ListPreviousName" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            $(".delete").click(function () {
                $("#hdnNameId").val($(this).attr("nameid"));
                $('#myModal').modal('show');
                return false;
            });

            $("#lnkDeleteConfirm").click(function () {
                $.ajax({
                    type: "POST",
                    url: "ListPreviousName.aspx/DeletePreviousData",
                    data: "{'NameId':'" + $("#hdnNameId").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (msg) {
                        if (msg.d == "true") {
                            window.open("ListPreviousName.aspx", "_self");
                        }
                        else {
                            alert("Something went wrong.");
                        }
                    },
                    error: function (data) {
                        alert("Something went wrong.");
                    }
                });
                return false;
            });
             
        });
         
    </script>

    <div class="row">
        <asp:HiddenField runat="server" ID="hdnNameId" ClientIDMode="Static" />
        <asp:CheckBox runat="server" ID="chkAppHasPreviosName" AutoPostBack="true" OnCheckedChanged="chkAppHasPreviosName_CheckedChanged" ClientIDMode="Static" Text="The applicant has previous names" />
        <br />
        <div id="divAddNewPreviousName" runat="server">
            <asp:Button runat="server" ID="btnAddPreviousName" ClientIDMode="Static" Text="Add a Previous Name"
                CssClass="btn btn-primary"
                OnClick="btnAddPreviousName_Click" />

            <asp:GridView ID="grvPreviousNameList" runat="server" AutoGenerateColumns="false" CssClass="table table-hover mt-10"
                ShowHeaderWhenEmpty="True" EmptyDataText="No names found">
                <Columns>
                    <asp:BoundField DataField="Title" HeaderText="Title" />
                    <asp:BoundField DataField="GivenName" HeaderText="Given Name" />
                    <asp:BoundField DataField="MiddleName" HeaderText="Middle Name" />
                    <asp:BoundField DataField="Surname" HeaderText="Surname" />
                    <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <a href="AddPreviousName.aspx?NameID=<%# Eval("NameID") %>">Edit</a>
                            <a ID="lnkDelete" nameid='<%# Eval("NameID") %>' class="delete" style="cursor:pointer;">Delete</a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to delete selected previous name?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="lnkDeleteConfirm">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
