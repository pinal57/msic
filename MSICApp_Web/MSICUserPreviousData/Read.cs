﻿using System;
using System.Collections.Generic;
using MSICApp.Return;
using System.Configuration;
using log4net;
using System.Data.SqlClient;
using System.Data;

namespace MSICApp_Web.MSICUserPreviousData
{
    public class Read
    {
        ILog Log = LogManager.GetLogger(typeof(Read));
        string ConnectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["MSICConnection"]);

        public Response<List<MsicPreviousName>> GetPreviousNameListByApplicationId(Int32 ApplicationId)
        {
            var response = new Response<List<MsicPreviousName>>
            {
                Message = "",
                StatusCode = -99,
                Content = null
            };

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                try
                {
                    cmd = connection.CreateCommand();

                    cmd.CommandText = " SELECT * FROM MsicPreviousNames WHERE ApplicationId = @ApplicationId ";
                    cmd.Parameters.AddWithValue("@ApplicationId", ApplicationId);

                    MsicPreviousName objSMsicPreviousName = null;
                    List<MsicPreviousName> iList = new List<MsicPreviousName>();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        objSMsicPreviousName = new MsicPreviousName();
                        objSMsicPreviousName.ApplicationId = Convert.ToInt32(reader["ApplicationId"]);
                        objSMsicPreviousName.NameId = Convert.ToInt32(reader["NameId"]);
                        objSMsicPreviousName.Title = Convert.ToString(reader["Title"]);
                        objSMsicPreviousName.GivenName = Convert.ToString(reader["GivenName"]);
                        objSMsicPreviousName.MiddleName = Convert.ToString(reader["MiddleName"]);
                        objSMsicPreviousName.Surname = Convert.ToString(reader["Surname"]);

                        iList.Add(objSMsicPreviousName);
                    }
                    reader.Close();
                    cmd.Dispose();
                    connection.Close();

                    response.StatusCode = iList != null ? 200 : 404;
                    response.Message = "";
                    response.Content = iList;
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("ERROR: ApplicationID:{0}, Source: {1}, Message: {2}",
                                    ApplicationId, "GetPreviousNameListByApplicationId()", ex.Message), ex);

                    return new Response<List<MsicPreviousName>>
                    {
                        StatusCode = -99,
                        Message = ex.Message.ToString(),
                        Content = null
                    };
                }
            }

            return response;
        }

        public Response<MsicPreviousName> GetPreviousNameListByAppIdAndNameId(Int32 ApplicationId, Int32 NameId)
        {
            var response = new Response<MsicPreviousName>
            {
                Message = "",
                StatusCode = -99,
                Content = null
            };

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                try
                {
                    cmd = connection.CreateCommand();

                    cmd.CommandText = " SELECT * FROM MsicPreviousNames WHERE ApplicationId = @ApplicationId AND NameId = @NameId ";
                    cmd.Parameters.AddWithValue("@ApplicationId", ApplicationId);
                    cmd.Parameters.AddWithValue("@NameId", NameId);

                    MsicPreviousName objSMsicPreviousName = null;

                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        objSMsicPreviousName = new MsicPreviousName();
                        objSMsicPreviousName.ApplicationId = Convert.ToInt32(reader["ApplicationId"]);
                        objSMsicPreviousName.NameId = Convert.ToInt32(reader["NameId"]);
                        objSMsicPreviousName.Title = Convert.ToString(reader["Title"]);
                        objSMsicPreviousName.GivenName = Convert.ToString(reader["GivenName"]);
                        objSMsicPreviousName.MiddleName = Convert.ToString(reader["MiddleName"]);
                        objSMsicPreviousName.Surname = Convert.ToString(reader["Surname"]);

                    }
                    reader.Close();
                    cmd.Dispose();
                    connection.Close();

                    response.StatusCode = objSMsicPreviousName != null ? 200 : 404;
                    response.Message = "";
                    response.Content = objSMsicPreviousName;
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("ERROR: ApplicationID:{0}, Source: {1}, Message: {2}",
                                    ApplicationId, "GetPreviousNameListByApplicationId()", ex.Message), ex);

                    return new Response<MsicPreviousName>
                    {
                        StatusCode = -99,
                        Message = ex.Message.ToString(),
                        Content = null
                    };
                }
            }

            return response;
        }
    }
}