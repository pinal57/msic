﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSICApp_Web.MSICUserPreviousData
{
    public class MsicPreviousName
    {
        public int ApplicationId { get; set; }
        public int NameId { get; set; }
        public string Title { get; set; }
        public string GivenName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
    }
}