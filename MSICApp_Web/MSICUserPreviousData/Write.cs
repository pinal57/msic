﻿using System;
using MSICApp.Return;
using System.Configuration;
using log4net;
using System.Data.SqlClient;
using System.Data;

namespace MSICApp_Web.MSICUserPreviousData
{
    public class Write
    {
        ILog Log = LogManager.GetLogger(typeof(Write));
        string ConnectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["MSICConnection"]);

        public Response SavePreviousName(MsicPreviousName objMsicPreviousName)
        {
            var response = new Response
            {
                Message = "",
                StatusCode = -99
            };

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                try
                {
                    cmd = connection.CreateCommand();

                    if (objMsicPreviousName.NameId != 0)
                    {
                        cmd.CommandText = " UPDATE MsicPreviousNames SET Title = @Title, GivenName = @GivenName, MiddleName = @MiddleName, Surname = @Surname  " +
                                            " WHERE ApplicationId = @ApplicationId AND NameId = @NameId ";
                    }
                    else
                    {
                        cmd.CommandText = " DECLARE @FNameID INT = 0; SELECT @FNameID = MAX(ISNULL(NameId, 0)) + 1 FROM MsicPreviousNames WHERE ApplicationId = @ApplicationId " +
                                           " INSERT INTO MsicPreviousNames (ApplicationId, NameId, Title, GivenName, MiddleName, Surname) " +
                                           " VALUES (@ApplicationId, ISNULL(@FNameID, 1), @Title, @GivenName, @MiddleName, @Surname) ";
                    }

                    cmd.Parameters.AddWithValue("@ApplicationId", objMsicPreviousName.ApplicationId);
                    cmd.Parameters.AddWithValue("@NameId", objMsicPreviousName.NameId);
                    cmd.Parameters.AddWithValue("@Title", objMsicPreviousName.Title);
                    cmd.Parameters.AddWithValue("@GivenName", objMsicPreviousName.GivenName);
                    cmd.Parameters.AddWithValue("@MiddleName", objMsicPreviousName.MiddleName);
                    cmd.Parameters.AddWithValue("@Surname", objMsicPreviousName.Surname);

                    cmd.ExecuteNonQuery();

                    cmd.Dispose();
                    connection.Close();

                    response.StatusCode = 200;
                    response.Message = "";
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("ERROR: ApplicationID:{0}, Source: {1}, Message: {2}",
                                    objMsicPreviousName.ApplicationId, "SavePreviousName()", ex.Message), ex);

                    return new Response
                    {
                        StatusCode = -99,
                        Message = ex.Message.ToString()
                    };
                }
            }

            return response;
        }

        public Response DeletePreviousName(Int32 ApplicationId, Int32 NameId)
        {
            var response = new Response
            {
                Message = "",
                StatusCode = -99
            };

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                try
                {
                    cmd = connection.CreateCommand();

                    cmd.CommandText = " DELETE FROM MsicPreviousNames WHERE ApplicationId = @ApplicationId AND NameId = @NameId ";

                    cmd.Parameters.AddWithValue("@ApplicationId", ApplicationId);
                    cmd.Parameters.AddWithValue("@NameId", NameId);

                    cmd.ExecuteNonQuery();

                    cmd.Dispose();
                    connection.Close();

                    response.StatusCode = 200;
                    response.Message = "";
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("ERROR: ApplicationID:{0}, NameId:{1}, Source: {2}, Message: {3}",
                                    ApplicationId, NameId, "DeletePreviousName()", ex.Message), ex);

                    return new Response
                    {
                        StatusCode = -99,
                        Message = ex.Message.ToString()
                    };
                }
            }

            return response;
        }
    }
}